<html <?php language_attributes(); ?>>
  <head>
    <meta name="theme-color" content="#111111">
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php wp_head(); ?>
  </head>
  <body <?php body_class(); ?> >
    <header class="site-nav">
      <div class="wrapper">
        <div class="site-logo-container">
          <a class="site-logo" href="<?php echo get_home_url(); ?>">
            <img src="<?php echo get_stylesheet_directory_uri() ?>/assets/img/logo.png" title="<?php echo bloginfo( 'name' ) ?>">
          </a>
        </div>
        <nav>
          <?php if ( has_nav_menu( 'primary' ) ) {
            $primary_args = array(
              'theme_location'  => 'primary',
              'menu_class'      => 'primary',
              'container'       => false,
            );
            wp_nav_menu( $primary_args );
          } ?>
        </nav>
      </div>
    </header>
    <button class="menu-button"><span class="v-hidden"><?php _e( 'Toggle Navigation', 'twenty-eighty' ); ?></span><i></i></button>
    <main>
