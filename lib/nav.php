<?php if ( !function_exists( 'te_register_menus' ) ) {
	function te_register_menus () {
		register_nav_menus( array(
			'primary' => 'Main Menu',
      'footer' => 'Footer Menu',
		) );
	}

	add_action( 'init', 'te_register_menus' );
}
