<?php get_header();
if( have_posts() ) {
  while( have_posts() ) {
    the_post();
    get_template_part( 'partials/page/title' );
    get_template_part( 'partials/page/content' );
  }
}
get_footer();
